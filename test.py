#!/usr/bin/python3
import rcontrolCore

rc = rcontrolCore.rcontrolCore()

_items = rc.listSession()
_items.sort()

for _item in _items:
	print(_item)

rc.notify("org.freedesktop.Notifications", "/org/freedesktop/Notifications", "PoC", "It works!")
