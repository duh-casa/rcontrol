import dbus

class rcontrolCore:

	def __init__(self):
		self._session_bus = dbus.SessionBus()
		self._system_bus = dbus.SystemBus()

	def listSession(self):
		return self._ommitColon(self._session_bus.list_names())

	def listSystem(self):
		return self._ommitColon(self._system_bus.list_names())

	def _ommitColon(self, _list):
		_out = []
		for _item in _list:
			if _item[0] != ":":
				_out.append(str(_item))
		return _out

	#https://wiki.archlinux.org/title/Desktop_notifications#Python
	def notify(self, service, objectPath, title, message):
		_obj = self._session_bus.get_object(service, objectPath)
		_obj = dbus.Interface(_obj, service)
		#https://hackage.haskell.org/package/fdo-notify-0.3.1/docs/DBus-Notify.html
		_obj.Notify("", 0, "", title, message, [], {"urgency": 1}, 1000)
