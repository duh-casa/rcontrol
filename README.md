# 2024-5 distributed PC

Duh časa project 2024-5 distributed PC. Software framework for easy functionality integration across a local network.

Aims to use existing solutions as much as possible, so is planned to only contain bits and bobs to make the experience seamless, for the example scenarios. Will also contain examples on how to extend functionality.

This project is in the early stages, README will be filled out more in the future.
